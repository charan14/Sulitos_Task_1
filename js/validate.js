function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isMobile(mobile) {
    var validateMobNum = /^\d*(?:\.\d{1,2})?$/;
    if (validateMobNum.test(mobile) && mobile.length == 10) {
        return true;
    }
    else {
        return false;
    }
}


$("#login").click(function(){
    var username = $("#username").val();
    var password = $("#password").val();

    if(username.length >= 4) {
        if (!/[^a-zA-Z]/.test(username)) {
            if (password.length >= 8) {
                if (!/^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{7,}$/.test(password)) {
                    M.toast({html: 'Sucess'});
                } else {
                    M.toast({html: 'Password should contain atleast one digit ,one lower case , one upper case'})
                }
            } else {
                M.toast({html: 'Password Should Be Minimum 8 Characters'});
            }
        } else {
            M.toast({html: 'Username Should contain Alphabets only'});
        }
    }else{
        M.toast({html: 'Username Should contain Minimum 4 Characters'})
    }

});
$("#register-u").click(function(){
    var username = $("#username").val();
    var mobile = $("#mobile").val();
    var email = $("#email").val();
    var address = $("#address").val();
    var bgroup = $("#blood-group").val();
    var groups = ['A+','A-','B+','B-','AB+','AB-','O+','O-'];

    if(username.length >= 4) {
        if (!/[^a-zA-Z ]/.test(username)) {
            if(mobile.length == 10){
                if(isMobile(mobile)){
                    if(isEmail(email)){
                        if(address.length !=0){
                            if (groups.indexOf(bgroup) > -1) {
                                M.toast({html: 'Sucess'});
                            } else {
                                M.toast({html: 'Enter Address'});
                            }
                        }else{
                            M.toast({html: 'Enter Address'});
                        }
                    }else{
                        M.toast({html: 'Enter a Valid Email'});
                    }
                }else{
                    M.toast({html: 'Mobile No Should contain Digits only'});
                }
            }else{
                M.toast({html: 'Mobile No Should contain Exact 10 Digits'});
            }
        } else {
            M.toast({html: 'Name Should contain Alphabets only'});
        }
    }else{
        M.toast({html: 'Name Should contain Minimum 4 Characters'})
    }

});


$("#register-h").click(function(){
    var username = $("#username-h");
    var blood = $("#blood-required");
    var address = $("#address-h");
    var name = $("#name-h");
    if(username.length >= 4) {
        if (!/[^a-zA-Z ]/.test(username)) {
            if(name.length >= 5) {
                if (!/[^a-zA-Z ]/.test(name)) {
                    if(address.length !=0){
                        if(!/[0-9]/.test(blood)){
                            M.toast({html: 'sucess'});
                        }else{
                            M.toast({html: 'Blood Required Should Be in Numeric'});
                        }
                    }else{
                        M.toast({html: 'Enter Address'});
                    }
                } else {
                    M.toast({html: 'Hospital Name Should contain Alphabets only'});
                }
            }else{
                M.toast({html: 'Hospital Name Should contain Minimum 5 Characters'});
            }
        } else {
            M.toast({html: 'Incharge Name Should contain Alphabets only'});
        }
    }else{
        M.toast({html: 'Incharge Name Should contain Minimum 4 Characters'});
    }
}